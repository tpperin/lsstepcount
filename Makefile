TARGET = :clang
ARCHS = armv7 armv7s arm64
THEOS_PACKAGE_DIR_NAME = pkgs

include theos/makefiles/common.mk

APPLICATION_NAME = LSStepCountApp
LSStepCountApp_FILES = main.m LSStepCountAppApplication.mm
LSStepCountApp_FRAMEWORKS = CoreMotion
LSStepCountApp_LIBRARIES = objcipc

TWEAK_NAME = LSStepCountTweak
LSStepCountTweak_FILES = Tweak.xm
LSStepCountTweak_FRAMEWORKS = UIKit CoreMotion
LSStepCountTweak_LIBRARIES = objcipc

include $(THEOS_MAKE_PATH)/application.mk
include $(THEOS_MAKE_PATH)/tweak.mk

before-stage::
	find . -name ".DS_Store" -delete
after-install::
	install.exec "killall -9 backboardd"
