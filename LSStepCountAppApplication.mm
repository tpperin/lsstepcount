#import "header.h"

@interface LSStepCountAppApplication: UIApplication <UIApplicationDelegate>
@end

@implementation LSStepCountAppApplication
- (void)applicationDidFinishLaunching:(UIApplication *)application {
	//Register for request for step count
	[OBJCIPC registerIncomingMessageFromSpringBoardHandlerForMessageName:@"LSStepCount" handler:^NSDictionary *(NSDictionary *dict) {
		//Once we receive a request, start getting the steps
		[self getSteps];
		return nil;
	}];
}

- (void)getSteps {
	//This is all your stuff pretty much
	NSDate *now = [NSDate date];
	NSCalendar *calendar = [NSCalendar currentCalendar];

	NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:now];

	[components setHour:0];
	[components setMinute:0];
	[components setSecond:0];

	NSDate *dayStart = [calendar dateFromComponents:components];

	CMStepCounter *stepCounter = [[CMStepCounter alloc] init];

	[stepCounter queryStepCountStartingFrom:dayStart
							to:now
						toQueue:[NSOperationQueue mainQueue]
					withHandler:^(NSInteger numberOfSteps, NSError *error) {
						NSDictionary *rep;
						if (error == nil) {
							rep = @{@"success":@(YES),@"steps":@(numberOfSteps)};
						}
						else {
							rep = @{@"success":@(NO),@"error":error};
						}
						//Send a reply with the result
						[self reply:rep];
					}];
}

- (void)reply:(NSDictionary *)dict {
	//Send the reply to SpringBoard where the tweak will pick it up
	[OBJCIPC sendMessageToSpringBoardWithMessageName:@"LSStepCountReply" dictionary:dict replyHandler:nil];
}

- (void)dealloc {
	[super dealloc];
}
@end
