//
//  Tweak.xm
//  LSSteps
//
//  Created by Timm Kandziora on 10.05.14.
//  Copyright (c) 2014 Timm Kandziora. All rights reserved.
//

#import "header.h"

@interface SBLockScreenView : UIView <UIScrollViewDelegate>
- (void)layoutSubviews;
@end

@interface SBStatusBarStateAggregator : NSObject {
	NSString* _timeItemTimeString;
}
-(void)_updateTimeItems;
- (void)_resetTimeItemFormatter;
@end;

static BOOL isLSEnabled = YES;
static BOOL isSBEnabled = YES;

static UILabel *label = nil;
static NSString *stepCount = nil;

static void UpdateStepCount() {
	if (!isLSEnabled && !isSBEnabled) return;
	[OBJCIPC sendMessageToAppWithIdentifier:@"in.timper.lsstepcountapp" messageName:@"LSStepCount" dictionary:nil replyHandler:nil];
}

static void SetSBText() {
	if (stepCount == nil) return; //dont show anything if we haven't received a step count yet
	SBStatusBarStateAggregator* sbagg = [%c(SBStatusBarStateAggregator) sharedInstance];
	NSDateFormatter *dateFormat = MSHookIvar<NSDateFormatter *>(sbagg, "_timeItemDateFormatter");

	NSString *newFormat = [NSString stringWithFormat:@"h:mm a [%@]",stepCount];

	[dateFormat setDateFormat:newFormat];
	[sbagg _updateTimeItems];
}

static void SetLSText() {
	label.text = [NSString stringWithFormat:@"[LSStepCount] Steps: %@",stepCount];
}

static void StepsUpdated() {
	if (isLSEnabled) SetLSText();
	if (isSBEnabled) SetSBText();
}

static void ReloadSettings() {
	NSMutableDictionary *settings = [[NSMutableDictionary alloc] initWithContentsOfFile:@"/var/mobile/Library/Preferences/in.timper.lsstepcount.plist"];
	if (settings) {
		if ([settings objectForKey:@"lsEnabled"]) {
			isLSEnabled = [[settings objectForKey:@"lsEnabled"] boolValue];
		}
		if ([settings objectForKey:@"sbEnabled"]) {
			BOOL temp = [[settings objectForKey:@"sbEnabled"] boolValue];
			if (!temp && isSBEnabled) {
				isSBEnabled = NO;
				//Remove the step count from the status bar
				SBStatusBarStateAggregator* sbagg = [%c(SBStatusBarStateAggregator) sharedInstance];
				NSDateFormatter *dateFormat = MSHookIvar<NSDateFormatter *>(sbagg, "_timeItemDateFormatter");
				[dateFormat setDateFormat:@"h:mm a"];
				[sbagg _updateTimeItems];
			}
			else if (temp && !isSBEnabled) {
				//Add the step count now
				isSBEnabled = YES;
				SetSBText();
			}
		}
	}
}

%hook SBLockScreenView

- (void)layoutSubviews
{
	if (!isLSEnabled) return %orig;

	UIView *frontView = MSHookIvar<UIView*>(self,"_slideToUnlockSpringView");
	if (!label) {
		label = [[UILabel alloc] initWithFrame:CGRectMake(10, 200, 300, 20)];
		label.textAlignment = NSTextAlignmentCenter;
		label.numberOfLines = 0;
		label.textColor = [UIColor whiteColor];
		label.text = @"[LSStepCount]";
	}
	if (![label superview] || [label superview] != frontView) {
		[frontView addSubview:label];
	}

	UpdateStepCount();

	%orig;
}

%end

//Reason we have to hook here is so we don't get overwritten
%hook SBStatusBarStateAggregator
- (void)_resetTimeItemFormatter {
	%orig;

	if (!isSBEnabled) return;

	SetSBText();
}
%end

%ctor {
	@autoreleasepool {
		CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(),
	      NULL,
	      (CFNotificationCallback)ReloadSettings,
	      CFSTR("in.timper.lsstepcount/reloadSettings"),
	      NULL,
	      CFNotificationSuspensionBehaviorCoalesce);

		ReloadSettings();

		UpdateStepCount();

		if ([CMStepCounter isStepCountingAvailable]) {
			[OBJCIPC registerIncomingMessageHandlerForAppWithIdentifier:@"in.timper.lsstepcountapp" andMessageName:@"LSStepCountReply" handler:^NSDictionary *(NSDictionary *reply) {
				if ([reply[@"success"] boolValue]) {
					//We received a successful reply without errors that has steps
					stepCount = reply[@"steps"];
					StepsUpdated();
				}
				return nil;
			}];
		} else {
			label.text = @"[LSStepCount] Device not supported.";
		}
	}
}
